//
//  FirstMonthCell.swift
//  MoleculusTest
//
//  Created by Admin on 12.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import YLProgressBar

class FirtsMonthCell: BaseTableCell {
    
    let lockImageView: UIImageView = {
        let iv = UIImageView(image: #imageLiteral(resourceName: "icn_services_science"))
        iv.contentMode = .scaleAspectFill
        return iv
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.text = "Первый месяц"
        label.font = .systemFont(ofSize: 16, weight: UIFontWeightRegular)
        return label
    }()
    
    let subTitleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.rgb(114, green: 119, blue: 122)
        label.text = "5 дней до следущего достижения"
        label.font = .systemFont(ofSize: 11, weight: UIFontWeightRegular)
        return label
    }()
    
    let progressBar: YLProgressBar = {
        
        let pb = YLProgressBar()
        pb.type = .rounded
        pb.hideGloss = true
        pb.hideStripes = true
        pb.progressBarInset = 0
        let rainbowColors: [UIColor] = [.red, .orange, .yellow, .green]
        pb.progressTintColors = rainbowColors
        pb.trackTintColor = UIColor.rgb(43, green: 55, blue: 68)
        pb.hideStripes = true
        pb.indicatorTextDisplayMode = .none
        pb.progress = 0.85
        pb.translatesAutoresizingMaskIntoConstraints = false
        pb.heightAnchor.constraint(equalToConstant: 5).isActive = true
        pb.progressStretch = false
        
        return pb
    }()
    
    let vericalStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        stackView.spacing = 8
        return stackView
    }()
    
    let arrowImageView: UIImageView = {
        let iv = UIImageView(image: #imageLiteral(resourceName: "further_arrow"))
        return iv
    }()
    
    override func setupViews() {
        
        backgroundColor = .clear
        
        addSubview(lockImageView)
        lockImageView.anchor(nil, left: leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 58, heightConstant: 58)
        lockImageView.anchorCenterYToSuperview()
        
        addSubview(vericalStackView)
        vericalStackView.anchor(nil, left: lockImageView.rightAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        vericalStackView.anchorCenterYToSuperview()
        vericalStackView.addArrangedSubview(titleLabel)
        vericalStackView.addArrangedSubview(progressBar)
        vericalStackView.addArrangedSubview(subTitleLabel)
        
        addSubview(arrowImageView)
        arrowImageView.anchor(nil, left: vericalStackView.rightAnchor, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 7, heightConstant: 10)
        arrowImageView.anchorCenterYToSuperview()
        
    }    
}
