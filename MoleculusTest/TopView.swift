//
//  TopView.swift
//  MoleculusTest
//
//  Created by Admin on 12.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class TopView: CustomView {

    let horizontalStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .fillProportionally
        stackView.spacing = 8
        return stackView
    }()
    
    let daysLabel: UILabel = {
        
        let label = UILabel()
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.paragraphSpacing = 1
        paragraphStyle.alignment = .center
        
        let combine = NSMutableAttributedString()
        combine.append(NSAttributedString(string: "25\n", attributes: [NSFontAttributeName : UIFont.systemFont(ofSize: 67, weight: UIFontWeightUltraLight)]))
        combine.append(NSAttributedString(string: "дней", attributes: [NSFontAttributeName : UIFont.systemFont(ofSize: 22, weight: UIFontWeightLight)]))
        combine.addAttributes([NSParagraphStyleAttributeName:paragraphStyle], range: NSRange(location: 0, length: combine.length))
        
        label.attributedText = combine
        
        return label
    }()
    
    let hoursLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.text = "10 часов"
        label.font = .systemFont(ofSize: 15, weight: UIFontWeightRegular)
        label.textAlignment = .center
        return label
    }()
    
    let minutesLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.text = "45 минут"
        label.font = .systemFont(ofSize: 15, weight: UIFontWeightRegular)
        label.textAlignment = .center
        return label
    }()
    
    let secondsLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.text = "57 секунд"
        label.font = .systemFont(ofSize: 15, weight: UIFontWeightRegular)
        label.textAlignment = .center
        return label
    }()
    
    override func setupViews() {
        
        addSubview(daysLabel)
        daysLabel.topAnchor.constraint(equalTo: topAnchor, constant: 64 + 10).isActive = true
        daysLabel.anchorCenterXToSuperview()
        
        addSubview(horizontalStackView)
        horizontalStackView.anchor(daysLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 26, leftConstant: 30, bottomConstant: 0, rightConstant: 30, widthConstant: 0, heightConstant: 0)
        horizontalStackView.addArrangedSubview(hoursLabel)
        horizontalStackView.addArrangedSubview(minutesLabel)
        horizontalStackView.addArrangedSubview(secondsLabel)
    }
}
