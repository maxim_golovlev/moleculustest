//
//  BotImagesCell.swift
//  MoleculusTest
//
//  Created by Admin on 12.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class BotImagesCell: BaseCollectionCell {
    
    static let greenColor = UIColor.rgb(207, green: 255, blue: 44)
    static let yellowColor = UIColor.rgb(255, green: 177, blue: 61)
    static let redColor = UIColor.rgb(255, green: 63, blue: 116)

    let iconViews = [IconView(color: BotImagesCell.greenColor, image: #imageLiteral(resourceName: "wind"), title: "89,09%"),
                     IconView(color: BotImagesCell.yellowColor, image: #imageLiteral(resourceName: "sigarette"), title: "45,06%"),
                     IconView(color: BotImagesCell.redColor, image: #imageLiteral(resourceName: "longs"), title: "0%"),
                     IconView(color: BotImagesCell.redColor, image: #imageLiteral(resourceName: "sex"), title: "2,64%"),
                     IconView(color: BotImagesCell.redColor, image: #imageLiteral(resourceName: "snakes"), title: "0,88%"),
                     IconView(color: BotImagesCell.redColor, image: #imageLiteral(resourceName: "heart"), title: "0,34%"),
                     IconView(color: BotImagesCell.redColor, image: #imageLiteral(resourceName: "tie"), title: "0,72%")]
    
    let topHorizontalStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .fillProportionally
        stackView.spacing = 33
        return stackView
    }()
    
    let botHorizontalStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .fillProportionally
        stackView.spacing = 33
        return stackView
    }()
    
    override func setupViews() {
        
        backgroundColor = .clear
        
        addSubview(topHorizontalStackView)
        addSubview(botHorizontalStackView)
        topHorizontalStackView.anchorCenterXToSuperview()
        botHorizontalStackView.anchorCenterXToSuperview()
        addConstraintsWithFormat("V:|-20-[v0]-22-[v1]", views: topHorizontalStackView, botHorizontalStackView)
        
        for i in 0...3 {
            topHorizontalStackView.addArrangedSubview(iconViews[i])
        }
        
        for i in 4...6 {
            botHorizontalStackView.addArrangedSubview(iconViews[i])
        }
    }
}
