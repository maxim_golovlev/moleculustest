//
//  IconView.swift
//  MoleculusTest
//
//  Created by Admin on 12.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class IconView: CustomView {
    
    let icon: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        return imageView
    }()
    
    let title: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 12, weight: UIFontWeightRegular)
        label.textAlignment = .center
        return label
    }()
    
    init(color: UIColor, image: UIImage, title: String) {
        super.init(frame: .zero)
        
        self.icon.tintColor = color
        self.title.textColor = color
        self.icon.image = image
        self.title.text = title
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setupViews() {
        addSubview(icon)
        addSubview(title)
        addConstraintsWithFormat("H:|[v0(47)]|", views: icon)
        addConstraintsWithFormat("H:|[v0(47)]|", views: title)
        addConstraintsWithFormat("V:|[v0(47)]->=5-[v1]|", views: icon, title)
        
    }
}
