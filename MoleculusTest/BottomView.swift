//
//  BottomView.swift
//  MoleculusTest
//
//  Created by Admin on 12.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class BottomView: CustomView {

    let cellId = "cellId"
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.register(BotImagesCell.self, forCellWithReuseIdentifier: self.cellId)
        cv.dataSource = self
        cv.delegate = self
        cv.backgroundColor = .clear
        cv.isPagingEnabled = true
        cv.showsHorizontalScrollIndicator = false
        return cv
    }()
    
    let bottomSeparator: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.alpha = 0.4
        return view
    }()
    
    let pageControl: UIPageControl = {
        let pc = UIPageControl()
        pc.numberOfPages = 3
        pc.currentPageIndicatorTintColor = .orange
        pc.pageIndicatorTintColor = UIColor.rgb(43, green: 55, blue: 68)
        return pc
    }()
    
    let gearButton: UIButton = {
        let button = UIButton(type: .system)
        button.setBackgroundImage(#imageLiteral(resourceName: "gear"), for: .normal)
        return button
    }()
    
    override func setupViews() {
        
        addSubview(collectionView)
        collectionView.anchor(topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 190)
        
        addSubview(bottomSeparator)
        bottomSeparator.anchor(collectionView.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0.5)
        
        addSubview(pageControl)
        pageControl.anchor(collectionView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15.5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 15)
        pageControl.bottomAnchor.constraint(lessThanOrEqualTo: bottomAnchor).isActive = true
        pageControl.anchorCenterXToSuperview()
        
        addSubview(gearButton)
        gearButton.anchor(nil, left: nil, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 14.5, widthConstant: 22.5, heightConstant: 23.8)
        gearButton.centerYAnchor.constraint(equalTo: pageControl.centerYAnchor).isActive = true
    }
}

extension BottomView: UIScrollViewDelegate {
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        let index = Int(targetContentOffset.pointee.x / bounds.width)
        pageControl.currentPage = index
    }
}


extension BottomView: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! BotImagesCell
        return cell
    }
}

extension BottomView: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: UIScreen.main.bounds.width, height: 190)
    }
}
