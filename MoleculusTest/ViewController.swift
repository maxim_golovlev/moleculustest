//
//  ViewController.swift
//  MoleculusTest
//
//  Created by Admin on 12.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let backImageView: UIImageView = {
        let iv = UIImageView(image: #imageLiteral(resourceName: "background"))
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        return iv
    }()
    
    let topView: TopView = {
        let view = TopView()
        return view
    }()
    
    let midView: MiddleView = {
        let view = MiddleView()
        return view
    }()
    
    let botView: BottomView = {
        let view = BottomView()
        return view
    }()
        
    let topSeparator: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.alpha = 0.4
        return view
    }()
    
    let bottomSeparator: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.alpha = 0.4
        return view
    }()    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
    }
    
    func setupViews() {
        
        view.backgroundColor = .white
        
        configureNavBar()
        
        view.addSubview(backImageView)
        backImageView.fillSuperview()
        
        view.addSubview(topView)
        topView.anchor(nil, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 250)
        topView.topAnchor.constraint(greaterThanOrEqualTo: view.topAnchor).isActive = true
        
        view.addSubview(midView)
        midView.anchor(topView.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 80)
        
        view.addSubview(topSeparator)
        topSeparator.anchor(nil, left: view.leftAnchor, bottom: midView.topAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0.5)
        
        view.addSubview(bottomSeparator)
        bottomSeparator.anchor(midView.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0.5)
        
        view.addSubview(botView)
        botView.anchor(midView.bottomAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 235)
    }
    
    func configureNavBar() {
        let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "back_icon").withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(backHandler))
        navigationItem.leftBarButtonItem = backButton
        
        navigationItem.title = "Не курю"
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIColor.white.as1ptImage()
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.view.backgroundColor = .clear
        navigationController?.navigationBar.backgroundColor = .clear
    }
    
    func backHandler() {
        
    }
}
